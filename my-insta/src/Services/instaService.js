export default class instaService {
    constructor() {
        this._apiBase = 'http://localhost:3000';
    }

    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`);
        if (!res.ok) {
            throw new Error(`Couldnt fetch ${url}; received ${res.status}`);
        }
        return await res.json();
    }

    getAllPosts = async (value) => {
        const res = await this.getResource(value);
        let result = [];
        for(let i = 0; i < res.myFriends.length; i++) {
            result.push(await this.getResource(`/${res.myFriends[i]}/`));
        }
        return result.map(item => item.posts);
    }

    getAllPhotos = async (value) => {
        const res = await this.getResource(value);
        return res.posts.map(this._transformPosts); 
    }

    getAllUsers = async (value) => {
        const res = await this.getResource(value);
        let result = [];
        for(let i = 0; i < res.myFriends.length; i++) {
            result.push(await this.getResource(`/${res.myFriends[i]}/`));
        }
        return result;
    }

    getCurUser = async (value) => {
        const res = await this.getResource(value);
        return res.myProfile;
    }

    getPass = async value => {
        const res = await this.getResource(value);
        return res.password;
    }
    
    _transformPosts = (post) => {
        return {
            src: post.src,
            alt: post.alt
        }
    }

    _transformUser = (post) => {
        return {
            src: post.src,
            alt: post.alt,
            name: post.name
        }
    }
}