import React, { Component } from 'react';
import Header from './Components/Header';
import Feed from './Components/Feed';
import Profile from './Components/Profile';
import Login from './Components/Login';
import Friend from './Components/Friend';
import NewPost from './Components/NewPost';
import CreateUser from './Components/CreateUser';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

export default class App extends Component {
  state = {
    loginStatus: false,
    loginName: ''
  }

  updateLogonStatus = (name) => {
    this.setState({
      loginStatus: true,
      loginName: name
    });
  }

  updateLogoutStatus = () => {
    this.setState({
      loginStatus: false,
      loginName: ''
    });
  }

  render() {
    return (
      <Router>
        <div className='App'> 
          <Header loginStatus={this.state.loginStatus} updateLogoutStatus={this.updateLogoutStatus}/>
          <Route path='/' component={() => <Feed loginName={this.state.loginName} />} exact/>
          <Route path='/login/' component={() => <Login loginStatus={this.state.loginStatus} updateLogonStatus={this.updateLogonStatus} />} exact/>
          <Route path='/profile/' component={() => <Profile loginName={this.state.loginName} />} exact/>
          <Route path='/friend/:name/' component={() => <Friend name=':name'/>} />
          <Route path='/addpost/' component={() => <NewPost loginName={this.state.loginName} />} exact />
          <Route path='/create-user/' component={CreateUser} exact />
        </div>
      </Router>
    );
  }
}