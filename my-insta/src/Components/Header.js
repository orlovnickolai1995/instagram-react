import React, { Component } from 'react';
import logo from '../logo.svg';
import {Link} from 'react-router-dom';

export default class Header extends Component {
    render() {
        let classNameIn = this.props.loginStatus ? 'none' : ''
        let classNameOut = this.props.loginStatus ? '' : 'none'

        return (
            <header>
                <div className='container h-flex'>
                    <Link to='/' className='logo'>
                        <img src={logo} alt='logo'></img>
                    </Link>
                    <nav className='links'>
                        <ul>
                            <li>
                                <Link to='/' className='menu__links'>Лента</Link>
                            </li>
                            <li>
                                <Link to='/profile/' className='menu__links'>Профиль</Link>
                            </li>
                            <li className={classNameIn}>
                                <Link to='/login/' className='menu__links'>Войти</Link>
                            </li>
                            <li className={classNameOut}>
                                <Link to='/login/' onClick={this.props.updateLogoutStatus} className='menu__links'>Выйти</Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
        )
    }
}