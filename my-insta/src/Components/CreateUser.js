import React, { Component } from 'react';

export default class CreateUser extends Component {

    state = {
        name : '',
        altname: '',
        photo: '',
        password: ''
    }

    setName = (event) => {
        this.setState({
            name: event.target.value
        })
    }

    setAlt = (event) => {
        this.setState({
            altname: event.target.value
        })
    }

    setPhoto = (event) => {
        this.setState({
            photo: event.target.value
        })
    }

    setPass = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    viewState = (event) => {
        event.preventDefault();
        const user = {
            [this.state.name]: {
                posts: [],
                myFriends: [],
                myProfile: [
                    {
                        name: this.state.name,
                        altname: this.state.altname,
                        photo: this.state.photo,
                        src: this.state.photo
                    }
                ],
                password: this.state.password
            }
        }
        this.pushToDatabase(user);
    }

    pushToDatabase = async (value) => {
        const options = {
            method: 'PUT',
            body: JSON.stringify(value),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
        await fetch(`http://localhost:3000/${this.state.name}/`, {
            headers: {  'Content-Type': 'application/json' },
            method: "POST",
            body: JSON.stringify(value)
          })

        await fetch(`http://localhost:3000/${this.state.name}/`, options)
            .then(res => res.json());
    }

    render() {
        return (
            <form onSubmit={this.viewState}>
                <label to='name'>Никнейм</label>
                <input name='name' type='text' onChange={this.setName} required></input>
                <label to='altname'>Имя</label>
                <input name='altname' type='text' onChange={this.setAlt} required></input>
                <label to='photo'>Ссылка на фото</label>
                <input name='photo' type='text' onChange={this.setPhoto} required></input>
                <label to='pass'>Пароль</label>
                <input name='pass' type='textarea' onChange={this.setPass} required></input>
                <input type='submit' value='Войти'></input>
            </form>
        )
    }
}