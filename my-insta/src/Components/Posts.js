import React, {Component} from 'react';
import User from './User';
import InstaService from '../Services/instaService';
import ErrorMessagge from './Error';

export default class Posts extends Component {
    InstaService = new InstaService();
    state = {
        posts: [],
        error: false
    }

    componentDidMount() {
        this.updatePosts(); 
    }

    updatePosts() {
        this.InstaService.getAllPosts(`/${this.props.loginName}/`)
            .then(this.onPostsLoaded)
            .catch(this.onError);
    }

    onPostsLoaded = (posts) => {
        this.setState({
            posts,
            error: false
        })
    }

    onError = (err) => {
        this.setState({
            error: true
        })
    }

    renderItems(arr) {
        let result = [];
        for (let i = 0; i < arr.length; i++) {
            arr[i].map((item) => {
                const {name, altname, photo, src, alt, descr, id} = item;
                
                return result.push(
                    <div key={id+100} className="post">
                        <User 
                            src={photo}
                            alt={altname} 
                            name={name}
                            min/>
                        <img src={src} alt={alt}></img>
                        <div className="post__name">
                            {name}
                        </div>
                        <div className="post__descr">
                            {descr}
                        </div>
                    </div>
                )
            })
        }
        return result;
    }

    render() {
        const {error, posts} = this.state;

        if (error) {
            return <ErrorMessagge />
        }

        const items = this.renderItems(posts);
        return (
            <div className='left'>
                {items}
            </div>
        )
    }
}