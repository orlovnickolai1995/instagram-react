import React, { Component } from 'react';
import User from './User';
import Palette from './Palette';
import InstaService from '../Services/instaService';
import ErrorMessagge from '../Components/Error';
import {Link} from 'react-router-dom';

export default class Profile extends Component {
    InstaService = new InstaService();

    state = {
        error: false,
        curUser: []
    }

    componentDidMount() {
        this.updateUser();
    }

    updateUser() {
        this.InstaService.getCurUser(`/${this.props.loginName}/`)
            .then(this.onCurUserLoaded)
            .catch(this.onError);
    }

    onError = () => {
        this.setState({
            error: true
        })
    }

    onCurUserLoaded = user => {
        this.setState({
            error: false,
            curUser: user
        })
    }

    renderUser(arr) {
        return arr.map(item => {
            const {src, alt, name} = item;
            return (
                <User 
                src={src}
                alt={alt}
                name={name} />
            )
        })
    }

    qwe = async () => {
        await fetch('http://localhost:3000/wiedzmin2121')
            .then(res => res.json())
            .then(res => console.log(res));
    }

    render() {
        const {error, curUser} = this.state;

        if(error) {
            return <ErrorMessagge />
        }

        const userName = this.renderUser(curUser);

        return (
            <div className='container profile'>
                {userName}            
                <Link to='/addpost/' className='menu__links'>Добавить пост</Link>   
                <Palette loginName={this.props.loginName}/>
            </div>
        )
    }
}