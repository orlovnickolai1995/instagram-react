import React, { Component } from 'react';
import Palette from './Palette';
import Users from './Users';

export default class Friend extends Component {
    
    render() {
        const user = window.location.href.split(":")[3];
        return (
            <div className='container feed'>
                 <div className='left'>
                    <Palette loginName={user} />
                </div>
                <div className='right'>
                    <h5>Друзья: {user}</h5>
                    <Users loginName={user}/>
                </div>
            </div>
        )
    }
}