import React, { Component } from 'react';
import '../Styles/Login.css';
import { Redirect, Link } from 'react-router-dom';
import InstaService from '../Services/instaService';

export default class Login extends Component {
    InstaService = new InstaService();

    state = {
        userName: '',
        password: '',
    }
    
    setLogin = (event) => {
        this.setState({
            userName: event.target.value,
        })
    }

    setPass = (event) => {
        this.setState({
            password: event.target.value 
        })
    }

    checkPass = async (event) => {
        event.preventDefault();
        let password = await this.InstaService.getPass(`/${this.state.userName}/`)
        if(this.state.password === password) {
            await this.props.updateLogonStatus(this.state.userName);
        }
    }

    render() {
        
        if (this.props.loginStatus) {
            return (
                <Redirect to='/'/>
            )
        }
        return (
            <React.Fragment>
                <form onSubmit={this.checkPass}>
                    <label to='user-name'>Имя пользователя</label>
                    <input name='user-name' type='text' required onChange={this.setLogin}></input>
                    <label to='password'>Пароль</label>
                    <input name='password' type='text' required onChange={this.setPass}></input>
                    <input type='submit' value='Войти'></input>
                </form>
                <div>
                    <h5>Если у Вас еще нет аккаунта, то создайте его =)</h5>
                    <Link to='/create-user/'>Создать аккаунт</Link>
                </div>
            </React.Fragment>
        )
    }
}