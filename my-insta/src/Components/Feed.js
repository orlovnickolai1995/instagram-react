import React from 'react';
import Users from './Users';
import Posts from './Posts';

const Feed = props => {
    return (
        <div className="container feed">
            <Posts loginName={props.loginName}/>
            <Users loginName={props.loginName}/>
        </div>
    );
}

export default Feed;