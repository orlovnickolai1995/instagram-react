import React, { Component } from 'react';
import InstaService from '../Services/instaService';
import ErrorMessagge from './Error';

export default class Posts extends Component {
    InstaService = new InstaService();
    state = {
        error: false,
        photos: []
    }

    componentDidMount() {
        this.updatePhotos();
    }
    
    updatePhotos() {
        this.InstaService.getAllPhotos(`/${this.props.loginName}/`)
            .then(this.onPhotosLoaded)
            .catch(this.onError)
    }

    onError = () => {
        this.setState({
            error: true
        })
    }

    onPhotosLoaded = (photos) => {
        this.setState({
            error: false,
            photos
        })
    }

    renderItems(arr) {
        return arr.map(item => {
            const {src, alt} = item
            return (
                <img src={src} alt={alt}></img>
            )
        })
    }

    render() {
        const {error, photos} = this.state;

        if (error) {
            return <ErrorMessagge />
        }

        const items = this.renderItems(photos);

        return (
            <div className='palette'>
                {items}
            </div>
        )
    }
}