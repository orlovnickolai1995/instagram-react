import React, { Component } from 'react';
import InstaService from '../Services/instaService';
import {Redirect} from 'react-router-dom';

export default class NewPost extends Component {
    InstaService = new InstaService();

    state = {
        // curUser: {},
        name: this.props.loginName,
        altname: this.props.loginName,
        photo: '',
        src: '',
        alt: '',
        descr: '',
        id: '',
        redirect: false
    }

    CurUser = {}

    componentDidMount() {
        this.updateDataBase();
    }

    setPhoto = event => {
        this.setState({
            photo: event.target.value,
            src: event.target.value
        })
    }

    setAlt = event => {
        this.setState({
            alt: event.target.value
        })
    }

    setDescr = event => {
        this.setState({
            descr: event.target.value
        })
    }

    setId = event => {
        this.setState({
            id: event.target.value
        })
    }

    viewState = async event => {
        const itemToPush = {
            name: this.state.name,
            altname: this.state.altname,
            photo: this.state.photo,
            src: this.state.src,
            alt: this.state.alt,
            descr: this.state.descr,
            id: this.state.id
        }
        this.CurUser.posts.push(itemToPush);
        event.preventDefault();
        this.pushToDatabase();
    }

    pushToDatabase =  async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(this.CurUser),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        await fetch(`http://localhost:3000/${this.props.loginName}`, options)
            .then(res => res.json());

        (async () => {
            try {
                await this.setState({
                    redirect: true
                })
            } catch(e) {
                alert(e);
            }
        })()
    }

    updateDataBase = async () => {
        let data = await fetch('http://localhost:3000/wiedzmin2121');
        let result = await data.json();
        this.CurUser = result
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to='/profile/'/>
            )
        }
        return (
            <form onSubmit={this.viewState}>
                <label to='src'>Путь к фото</label>
                <input name='src' type='text' onChange={this.setPhoto} required></input>
                <label to='alt'>Alt для картинки</label>
                <input name='alt' type='text' onChange={this.setAlt} required></input>
                <label to='descr'>Подпись</label>
                <textarea name='descr' type='textarea' onChange={this.setDescr} required></textarea>
                <label to='id'>Уникальный id</label>
                <input name='id' type='input' onChange={this.setId} required></input>
                <input type='submit' value='Войти'></input>
            </form>
        )
    }
}