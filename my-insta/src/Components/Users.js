import React, { Component } from 'react';
import User from './User';
import InstaService from '../Services/instaService';
import ErrorMessagge from './Error';

export default class Users extends Component {
    InstaService = new InstaService();
    state = {
        users: [],
        error: false
    }

    componentDidMount() {
        this.updateUsers(); 
    }

    updateUsers() {
        this.InstaService.getAllUsers(`/${this.props.loginName}/`)
            .then(this.onUsersLoaded)
            // .cath(this.onError);
    }

    onUsersLoaded = (users) => {
        this.setState({
            users,
            error: false 
        })
    }

    onError = (err) => {
        this.setState({
            error: true
        })
    }

    renderUsers(arr) {
        return arr.map(item => {
            const {name, photo, alt} = item.myProfile[0];
            return (
                <User 
                    src={photo}
                    alt={alt} 
                    name={name}
                    min/>
            )
        })
    }

    render() {
        const {error, users} = this.state;
        
        if (error) {
            return <ErrorMessagge />
        }

        const items = this.renderUsers(users);
        return (
            <div className='right'>
                {items}
            </div>
        )
    }
}